paceOptions = {
   elements: false,
   restartOnPushState: false,
   restartOnRequestAfter: false
}

$("body").on("click",".xhr",function(){
   var url = $(this).attr('href');
   if(url!='' || url!=undefined){
      $("#my-content").load(url);
   }
   return false;
});

$("body").on("submit",".xhr_form",function(){
   var data = new FormData(this);
   var action = $(this).attr('action');
   $.ajax({
      data: data,
      type: "post",
      dataType:"json",
      url: action,
      contentType: false,
      processData: false,
      success: function(response){
         $("#my-content").load(response.redirect);
      }
   });
   return false;
});

$(document).ajaxStart(function() { 
   Pace.restart(); 
});

function Alert(msg,type){
   $.wnoty({
      type: type, // 'success', 'info', 'error', 'warning'
      message: msg,
      position: 'top-right'
	});
}