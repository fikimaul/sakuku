   </div>
   <!-- /.content-wrapper -->

   <footer class="main-footer">
      <div class="pull-right hidden-xs">
         <b>Version</b> 1.00-Beta
      </div>
      <strong>Copyright © 2019 <a href="https://fikimaul.github.io" target="_blank">FIXIT-LAB</a>.</strong> All rights reserved.
   </footer>
</div>
<!-- ./wrapper -->
<!-- start: Javascript -->
<script src="<?= base_url('asset/jquery/jquery.min.js')?>"></script>
<script src="<?= base_url('asset/jqueryui/jquery-ui.min.js')?>"></script>
<script src="<?= base_url('asset/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?= base_url('asset/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>

<script src="<?= base_url('asset/bootstrap/js/bootstrap.min.js')?>"></script>
<script src="<?= base_url('asset/adminlte2/js/adminlte.min.js')?>"></script>
<script src="<?= base_url('asset/pacejs/js/pace.min.js')?>"></script>
<script src="<?= base_url('asset/wnoty/wnoty.js')?>"></script>
<script src="<?= base_url('asset/jsku/jsku.js')?>"></script>
<!-- end: Javascript -->
</body>
</html>