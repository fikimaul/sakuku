<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="keyword" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
   <title><?= $this->config->item('app_title') ?></title>
   <!-- start: Css -->
   <link rel="stylesheet" type="text/css" href="<?= base_url('asset/bootstrap/css/bootstrap.min.css')?>">
   <link rel="stylesheet" type="text/css" href="<?= base_url('asset/adminlte2/css/AdminLTE.min.css')?>">
   <link rel="stylesheet" type="text/css" href="<?= base_url('asset/adminlte2/css/skins/skin-green.min.css')?>">
   <link rel="stylesheet" type="text/css" href="<?= base_url('asset/sweetalert/sweetalert.css')?>">
   <link rel="stylesheet" type="text/css" href="<?= base_url('asset/font-awesome/css/font-awesome.min.css')?>">
   <link rel="stylesheet" type="text/css" href="<?= base_url('asset/ionicons/css/ionicons.min.css')?>">
   <link rel="stylesheet" type="text/css" href="<?= base_url('asset/datatables.net-bs/css/dataTables.bootstrap.min.css')?>">
   <link rel="stylesheet" type="text/css" href="<?= base_url('asset/pacejs/css/pace.css')?>">
   <link rel="stylesheet" type="text/css" href="<?= base_url('asset/wnoty/wnoty.css')?>">
   <!-- Google Font -->
   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
   <!-- end: Css -->

   <script src="<?= base_url('asset/sweetalert/sweetalert.min.js')?>"></script>
   <link rel="shortcut icon" href="">
</head>
<body class="hold-transition skin-green fixed sidebar-mini">
<div class="wrapper">
   <header class="main-header">
      <!-- Logo -->
      <a href="<?= site_url('dashboard')?>" class="logo">
         <!-- mini logo for sidebar mini 50x50 pixels -->
         <span class="logo-mini"><b>SKK</B></span>
         <!-- logo for regular state and mobile devices -->
         <span class="logo-lg"><b>SAKUKU</b></span>
      </a>

      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
         <!-- Sidebar toggle button-->
         <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
         </a>
         <!-- Navbar Right Menu -->
         <div class="navbar-custom-menu">
         <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown">
               <img src="<?= base_url('asset/img/user2-160x160.jpg')?>" class="user-image" alt="User Image">
               <span class="hidden-xs"><?= $user_nama ?></span>
               </a>
               <ul class="dropdown-menu">
               <!-- User image -->
               <li class="user-header">
                  <img src="<?= base_url('asset/img/user2-160x160.jpg')?>" class="img-circle" alt="User Image">
                  <p>
                     <?= $user_nama ?>
                  </p>
               </li>
               <!-- Menu Footer-->
               <li class="user-footer">
                  <div class="pull-left">
                     <a href="<?= site_url('user/ViewProfil')?>" class="btn btn-default btn-flat xhr">Profile</a>
                  </div>
                  <div class="pull-right">
                     <a href="<?= site_url('login/Logout')?>" class="btn btn-default btn-flat">Keluar</a>
                  </div>
               </li>
               </ul>
            </li>
            <li>
         </ul>
         </div>

      </nav>
   </header>
   <!-- Left side column. contains the logo and sidebar -->
   <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
         <!-- sidebar menu: : style can be found in sidebar.less -->
         <ul class="sidebar-menu" data-widget="tree">
            <li>
               <a class="xhr" href="<?= site_url('dashboard/xhr')?>">
                  <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                  </span>
               </a>
            </li>
            <li class="treeview">
               <a href="#">
                  <i class="fa fa-plus-circle"></i> <span>Referensi</span>
               </a>
               <ul class="treeview-menu">
                  <li>
                     <a href="<?= site_url('kategori/ViewKategori')?>" class="xhr"><i class="fa fa-circle-o"></i>Kategori</a>
                  </li>
               </ul>
            </li>
            <li>
               <a class="xhr" href="<?= site_url('transaksi/ViewListTransaksi')?>">
                  <i class="fa fa-dashboard"></i> <span>Transaksi</span>
                  </span>
               </a>
            </li>
         </ul>
      </section>
      <!-- /.sidebar -->
   </aside>
      <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper"  id="my-content">
      
