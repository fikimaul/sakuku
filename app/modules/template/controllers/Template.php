<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template extends MX_Controller {
	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('user_id')=="") {
			redirect('login');
		}
	}

	public function index($view = 'template',$data = array())
	{
      $atas['user_nama'] = $this->session->userdata('user_nama');
      $this->load->view('atas', $atas);
      $this->load->view($view,$data);
      $this->load->view('bawah');
	}
}
