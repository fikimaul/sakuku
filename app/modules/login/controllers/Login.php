<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {
	public function __construct() {
		parent::__construct();
		
		$this->load->model('user/User','ObjUser');
	}

	public function index()
	{
		if ($this->session->userdata('user_id')!="") {
			redirect('dashboard');
		}
		
		if(isset($_GET['failed']))
			$data['failed'] = true;
		else
			$data['failed'] = false;
			
		$this->load->view('login',$data);
	}

	public function Auth(){
		$data = array(
				'user_username' => $this->input->post('username', TRUE),
				'user_password' => md5($this->input->post('password', TRUE))
			);
			
		$hasil = $this->ObjUser->cekUser($data);
		if (count($hasil) == 1) {
			extract($hasil[0]);

			$sess_data['user_id'] = $user_id;
			$sess_data['user_username'] = $user_username;
			$sess_data['user_nama'] = $user_fullname;
			$this->session->set_userdata($sess_data);
			
			redirect('dashboard');
		}else {
			redirect('login?failed=1');
		}
	}

	public function Logout(){
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('user_username');
		$this->session->unset_userdata('user_nama');
		session_destroy();
		redirect('login');
	}
}
