<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="keyword" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
   <title><?= $this->config->item('app_title') ?></title>
   <!-- start: Css -->
   <link rel="stylesheet" type="text/css" href="<?= base_url('asset/bootstrap/css/bootstrap.min.css')?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('asset/adminlte2/css/AdminLTE.min.css')?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('asset/sweetalert/sweetalert.css')?>">
   <!-- end: Css -->
   <script src="<?= base_url('asset/sweetalert/sweetalert.min.js')?>"></script>

   <link rel="shortcut icon" href="">
</head>
<body>
<body class="hold-transition login-page">
	<div class="login-box">
		<div class="login-logo">
			<b>SAKUKU</b> Login</a>
		</div>
		<!-- /.login-logo -->
		<div class="login-box-body">
			<p class="login-box-msg">Silahkan Login</p>
			<form action="<?= site_url('login/Auth')?>" method="post">
				<div class="form-group has-feedback">
					<input type="Username" name="username" class="form-control" placeholder="Username" required>
				<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="password" name="password" class="form-control" placeholder="Password" required>
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="row">
				<!-- /.col -->
				<div class="col-xs-4">
					<button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
				</div>
				<!-- /.col -->
				</div>
			</form>
		</div>
	<!-- /.login-box-body -->
	</div>
<!-- start: Javascript -->
<script src="<?= base_url('asset/jquery/jquery.min.js')?>"></script>
<script src="<?= base_url('asset/bootstrap/js/bootstrap.min.js')?>"></script>
<script src="<?= base_url('asset/jsku/jsku.js')?>"></script>
<?php
   if($failed){
      echo '<script>swal("Gagal", "Nama Pengguna atau Kata Sandi Salah!", "error");</script>';
   }
?>
<!-- end: Javascript -->
</body>
</html>