<?php
   if(!empty($msg)){
      echo '<script>Alert("'.$msg[0].'", "'.$msg[1].'");</script>';
   }
?>
<section class="content-header">
   <a class="xhr btn btn-md btn-default" href="<?= site_url('transaksi/ViewListTransaksi')?>" title="refresh">
      <i class="fa fa-refresh"></i>
   </a>
   <h1 class="pull-right">
      Daftar Transaksi
   </h1>  
</section>      
<!-- Main content -->
<section class="content">
   <div class="box box-default color-palette-box">
      <div class="box-header with-border">
         <a class="xhr btn btn-success" href="<?= site_url('transaksi/ViewInputTransaksi')?>" title="Tambah">
            <i class="fa fa-plus-circle"></i> Tambah
         </a>
      </div>
      <div class="box-body">
         <table id="transaksi" class="table table-bordered table-hover">
            <thead>
               <tr>
                  <th>NO</th>
                  <th>Kategori</th>
                  <th>Waktu</th>
                  <th>Deskripsi</th>
                  <th>Nominal</th>
                  <th width="30px">Aksi</th>
               </tr>
            </thead>
            <tbody>
               <?php 
                  $no = 1;
                  foreach($dataTransaksi as $value):
                     extract($value);
               ?>
               <tr>
                  <td><?= $no ?></td>
                  <td><?= $kategori_nama ?></td>
                  <td><?= $transaksi_waktu ?></td>
                  <td><?= $transaksi_deskripsi ?></td>
                  <td align="right"><?= uang($transaksi_nominal) ?></td>
                  <td align="center"><a href="<?= site_url('transaksi/ViewInputTransaksi?data_id=').$transaksi_id?>" class="xhr btn btn-xs   btn-info"> <i class="fa fa-edit"></i></a></td>
               </tr>
               <?php 
                  $no++;
                  endforeach;
               ?>
            </tbody>
         </table>
      </div>
   </div>
   </section>
<!-- /.content -->
<script>
   $(function () {
      $('#transaksi').DataTable();
   });
</script>