<?php
   $transaksi_kategori_id = "";
   $transaksi_deskripsi = "";
   $transaksi_nominal = "";
   $transaksi_id = NULL;

   if(!empty($msg)){
      echo '<script>Alert("'.$msg[0].'", "'.$msg[1].'");</script>';
   }
   if(!empty($data)){
      extract($data);
   }
?>
<section class="content-header">
   <a class="xhr btn btn-md btn-default" href="<?= site_url('transaksi/ViewListTransaksi')?>" title="refresh">
      <i class="fa fa-arrow-circle-left"></i>
   </a>
   <h1 class="pull-right">
      Transaksi
   </h1>  
</section>      
<!-- Main content -->
<section class="content">
   <div class="box box-default color-palette-box">
      <div class="box-header with-border">
      <h3 class="box-title"><?= is_null($transaksi_id) ? "Tambah" : "Ubah";?></h3> Transaksi
      </div>
      <div class="box-body">
         <form method="POST" name="frmTransaksi" action="<?= site_url('transaksi/DoSaveTransaksi')?>" class="xhr_form form-horizontal" id="frmTransaksi" >
            <input type="hidden" name="data_id" value="<?= $transaksi_id ?>">
            <div class="form-group">
               <label class="col-sm-2 control-label">
                  Kategori
               </label>
               <div class="col-sm-5">
                  <select name="kategori" class="form-control"> 
                     <?php foreach($cbxKategori as $value):?>
                        <option value="<?= $value['kategori_id']?>" <?php if($value['kategori_id']==$transaksi_kategori_id) echo "selected"?>><?= $value['kategori_nama'] ?></option>
                     <?php endforeach;?>
                  </select>
               </div>
            </div>
            <div class="form-group">
               <label class="col-sm-2 control-label">
                  Deskripsi
               </label>
               <div class="col-sm-5">
                  <textarea name="deskripsi" rows="2" valign="left" class="form-control" placeholder="Deskripsi"><?= $transaksi_deskripsi?></textarea>
               </div>
            </div>
            <div class="form-group">
               <label class="col-sm-2 control-label">
                  Nominal 
               </label>
               <div class="col-sm-5">
                  <input type="text" name="nominal" class="form-control" placeholder="Nominal" value="<?= $transaksi_nominal?>">
               </div>
            </div>
            <div class="form-group">
               <label class="col-sm-2 control-label">
                  File 
               </label>
               <div class="col-sm-5">
                  <input type="file" name="coba" class="form-control">
               </div>
            </div>
            <div class="col-md-10 col-md-offset-2">
               <input type="submit" name="btnsimpan" value=" Simpan " class="btn btn-primary">
               <a href="<?= site_url('transaksi/ViewListTransaksi')?>" class="xhr btn btn-warning" type="reset"> Batal</a>
            </div>
         </form>
      </div>
   </div>
</section>
<!-- /.content -->