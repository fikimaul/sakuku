<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ViewInputTransaksi extends MX_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('kategori/Kategori','ObjKategori');
      $this->load->model('transaksi/Transaksi','ObjTransaksi');
      
	}

	public function index()
	{
      $cbxKategori = $this->ObjKategori->getComboboxKategori();
		
		if(isset($_GET['data_id'])){
			$id = $_GET['data_id'];
			$data = $this->ObjTransaksi->getTransaksiById($id);
		}
		
      if($this->session->flashdata('msg')!=null){
			$msg 	= $this->session->flashdata('msg');
			$data = $msg[2];
			unset($msg[2]);
		}

		$this->load->view('input_transaksi',compact('cbxKategori','msg','data'));
	}
}
