<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ViewListTransaksi extends MX_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('transaksi/Transaksi','ObjTransaksi');
	}

	public function index()
	{
		if($this->session->flashdata('msg')!=null){
			$msg = $this->session->flashdata('msg');
		}

		$dataTransaksi = $this->ObjTransaksi->getListTransaksi();
		$this->load->view('list_transaksi', compact('dataTransaksi','msg'));
	}
}
