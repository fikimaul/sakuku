<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DoSaveTransaksi extends MX_Controller {
   
	public function __construct() {
		parent::__construct();
      $this->load->model('transaksi/Transaksi','ObjTransaksi');
	}
   
   public function CekData(){
      $msg = array();
      $data = $this->input->post(NULL,TRUE);

      if(!empty($data)){
         if($data['kategori'] == "")
            $msg[] = "Silahkan Pilih Kategori!";
         if($data['deskripsi'] == "")
            $msg[] = "Deskripsi Tidak Boleh Kosong!";
         if($data['nominal'] == "")
            $msg[] = "Nominal Tidak Boleh Kosong!";
         
      }

      return compact('msg','data');
   }

	public function index()
	{
      $cek = $this->CekData();
      extract($cek);
      $dataDb['transaksi_kategori_id'] 	= $data['kategori'];
      $dataDb['transaksi_deskripsi'] 		= $data['deskripsi'];
      $dataDb['transaksi_nominal']  		= $data['nominal'];
		$dataDb['transaksi_user_id']			= $this->session->userdata('user_id');
      $dataDb['transaksi_waktu']			   = date('Y-m-d H:i:s');
      $dataDb['transaksi_id']             = empty($data['data_id']) ? NULL : $data['data_id'];
         
      if(empty($msg)){
         if($dataDb['transaksi_id'] == NULL ){
   			$simpan = $this->ObjTransaksi->simpanTransaksi($dataDb);
            $aksi   = "Penambahan";
         }else{
            $simpan = $this->ObjTransaksi->updateTransaksi($dataDb);
            $aksi   = "Perubahan";
         }

			if($simpan){
            $this->UploadFile();
				$this->session->set_flashdata('msg',array($aksi.' Transaksi Sukses','success'));
				$return['redirect'] = 'transaksi/ViewListTransaksi';
			}else{
				$this->session->set_flashdata('msg',array($aksi.' Transaksi Gagal','error',$dataDb));
				$return['redirect'] = 'transaksi/ViewInputTransaksi';
         }
         
      }else{
			$this->session->set_flashdata('msg',array($msg[0],'error',$dataDb));
			$return['redirect'] = 'transaksi/ViewInputTransaksi';
		}

		echo json_encode($return);
   }
   
   private function UploadFile()
   {
      $config['upload_path']          = './upload/';
      $config['overwrite']			     = true;
      $config['allowed_types']        ='*';
      // $config['max_width']            = 1024;
      // $config['max_height']           = 768;

      $this->load->library('upload', $config);
      $this->upload->do_upload('coba');
   }
}
