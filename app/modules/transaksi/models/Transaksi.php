<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Model {
   
   public function getListTransaksi(){
      $this->db->select('*');
      $this->db->from('transaksi');
      $this->db->join('kategori','transaksi_kategori_id=kategori_id','left');
      $this->db->join('jenis','jenis_id=kategori_jenis_id','left');
      $this->db->where(array('transaksi_user_id'=>$this->session->userdata('user_id')));
      $result =  $this->db->get();

      return $result->result_array();
   }

   public function getTransaksiById($id){
      $where = array("transaksi_id"=>$id);
      $result = $this->db->get_where('transaksi',$where)->result_array();

      return $result[0];
   }

   public function simpanTransaksi($data){
      $result = $this->db->insert("transaksi",$data);
      
      return $result;
   }
   
   public function updateTransaksi($data){
      $where = array("transaksi_id"=>$data['transaksi_id']);
      unset($data['transaksi_id']);

      $result = $this->db->update("transaksi",$data,$where);
      
      return $result;
   }
}
