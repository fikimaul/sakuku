<section class="content-header">
   <a class="xhr btn btn-md btn-default" href="<?= site_url('kategori')?>" title="refresh">
      <i class="fa fa-refresh"></i>
   </a>
   <h1 class="pull-right">
      Kategori
   </h1>  
</section>
<!-- Main content -->
<section class="content">   
   <div class="box box-default color-palette-box">
      <div class="box-header with-border">
         <a class="xhr btn btn-success" href="<?= site_url('kategori/ViewInputKategori')?>" title="Tambah">
            <i class="fa fa-plus-circle"></i> Tambah
         </a>
      </div>
      <div class="box-body">
         
      </div>
   </div>
</section>