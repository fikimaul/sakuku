<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Model {
   
   public function getComboboxKategori(){
      $this->db->select('*');
      $this->db->from('kategori');
      $this->db->join('jenis','jenis_id=kategori_jenis_id','left');
      $result = $this->db->get()->result_array();

      return $result;
   }
 
}