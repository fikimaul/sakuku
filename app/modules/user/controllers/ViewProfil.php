<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ViewProfil extends MX_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('user/User','UserModel');

	}

	public function index()
	{
		$detailUser = $this->UserModel->getUserById($this->session->userdata('user_id'));
		
		$this->load->view('user_profile',$detailUser);
	}
}
