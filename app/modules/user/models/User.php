<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Model {
   
   //untuk autentikasi login
	public function cekUser($data){
      $query = $this->db->get_where('user', $data);
		return $query->result_array();
   }

   public function getUserById($id){
      $where = array("user_id"=>$id);
      $result = $this->db->get_where('user',$where)->result_array();

      return $result[0];
   }

}
