<div class="box box-default color-palette-box">
   <div class="box-header with-border">
      <h3 class="box-title"><i class="fa fa-dashboard"></i> Profil</h3>
   </div>
   <div class="box-body">
      <dl class="dl-horizontal">
         <dt>Username</dt>
         <dd><?= $user_username ?></dd>
         <dt>Nama Lengkap</dt>
         <dd><?= $user_fullname ?></dd>
         <dt>Email</dt> <dd><?= $user_email ?></dd>
      </dl>
   </div>
</div>