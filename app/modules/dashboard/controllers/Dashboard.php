<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {
	public function __construct() {
		parent::__construct();
	}

	public function index()
	{
		echo Modules::run('template','dashboard/dashboard_view');
	}

	public function xhr()
	{
		$this->load->view('dashboard/dashboard_view');
	}
}
